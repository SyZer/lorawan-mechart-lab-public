#Grosi Alarm
An ethical tracking watch for dementia sufferers. We use geofencing to minimize data collection and thus allow the Grosi freedom.

#Product:

Grosi Alarm is an ethical GPS-tracking and emergency watch especially for dementia and Alzheimer’s sufferers living at home.

We created a fully working prototype using Low Power Network, Swisscom Application Cloud and iBeacon.

It has two main fully working functions on the prototype:

#Emergency Alarm:

The Grosi (Grandmother in Swiss German :simple_smile: ) can use the switch to sound the alarm, outside or inside wherever there is LoRa coverage. The alarm and GPS-position is transmitted over the Swisscom Low Power Network / LoRa to a fully working dashboard. The transmission of the alarm is signaled with a red LED on the prototype. The dashboard can be used for example by the Swiss Red Cross or also relatives. Further, and SMS is sent to a configurable number. The alarm then can be acknowledged on the dashboard.

##Ethical and legally compliant tracking with geofencing:

Simon is one of the team members. He had a grandmother that was suffering from mild dementia and living in a small village outside of the Swiss City of Aarau. She lived alone and was still very mobile. In her village she still knew her way would move around freely and could always find her way home. But every few months she would go to Aarau and got completely lost. The police sometimes had to be called. She was luckily always found. But there have been cases like this, where the dementia sufferer froze to death in winter.

We want to prevent that while not track too intrusively.

So whenever the Grosi suffering from dementia is within a predefined area where she knows her way - a geofence - no GPS-tracking data is logged. The watch only sends a signal that it is within the geofence. We do not want to know what she does every day. This is also ethical and compliant with Swiss and EU data protection laws. Personal data should only be collected as far as it is absolutely necessary. This is called data minimization.

The geofence can be configured over the air via LPN. When the Grosi is at home (no GPS signal inside!) she is checked in via iBeacon, and the GPS then can be turned off to save power. As soon as she leaves the geofence an alarm is triggered on the dashboard. An SMS is sent with the position where the Grosi left the fence, and logging starts.


=======================================================================================================================
this is second try of frameworks


#HOW

clone
cd
npm i
localhost:3000


#TODO
##spam Iwan


```
curl -X POST -H "Content-Type: application/json" localhost:3000/sms -d '{"message":"awesome"}'
curl -X POST -H "Content-Type: application/json" localhost:3000/sms -d '{"message":"awesome", "to":"+41779869000"}'
```


## Protocol

PHP Gateway -> Web app
```
curl "https://mechart-syzer.c9users.io/messages/" -d '{"lat": 42.12, "lng": 7.12, "date":"2015-11-07 11:02:58"}' -H "Content-Type: application/json"
curl "https://mechart-syzer.c9users.io/alarms/" -d '{"alarm": "button"}' |outsideGeofence|null -H "Content-Type: application/json"
curl "http://52.17.54.92:3000/ibeacons/" -d '{"state": "present"}' |absent
```

```
curl "0.0.0.0:3000/messages" -d '{"lat": 42.12, "lng": 7.12, "date":"2015-11-07 11:02:58"}' \
-H "Content-Type: application/json"
//=> 200
curl -X GET "0.0.0.0:3001/messages"
//=>{"{\"lat\": 42.12, \"lng\": 7.12, \"date\":\"2015-11-07 11:02:58\"}":""}
```

Web app -> PHP Gateway
```
curl "http://merchart-test.scapp.io/actility/geofence.php" -d '{"latBL":47.38266026, "lngBL":8.48462105, "latTR":47.4083412, "lngTR":8.55826378}'
curl "http://merchart-test.scapp.io/actility/resetAlarm.php" -X GET
```

##Fronend -> web app
```
curl -X POST "0.0.0.0:3000/geofences" -H "Content-Type: application/json" \
-d '{"latBL":47.38266026, "lngBL":8.48462105, "latTR":47.4083412, "lngTR":8.55826378}'
//=>
//{"name":"RequestError","message":"Error: options.uri is a required argument","cause":{},"error":{},"options":{"json":true,"body":{"latBL":47.38266026,"lngBL":8.48462105,"latTR":47.4083412,"lngTR":8.55826378},"method":"POST","simple":true,"resolveWithFullResponse":false}}⏎
```

##lowowan->frontend
```
curl "0.0.0.0:3000/alarms" -H "Content-Type: application/json" \
-d '{"alarm":"button"}'
```
